Categories:Money
License:GPLv3
Web Site:http://greenaddress.com
Source Code:https://github.com/greenaddress/GreenBits
Issue Tracker:https://github.com/greenaddress/GreenBits/issues
Changelog:https://github.com/greenaddress/GreenBits/releases

Auto Name:GreenBits
Summary:GreenAddress Testnet client
Description:
GreenBits Testnet is a Bitcoin Wallet for Android provided by GreenAddress for
the Testnet network.

The wallet main strengths are security, privacy, easy of use and aims is to
provide a great user experience.

* It supports the payment protocol via both click and qrcode scanning and instant confirmation.
* Supports paper wallet scanning in both WIF and BIP38 format.
* Uses multisignature for improved security and per transaction two factor authentication.

Please note that another GreenAddress app is available for Android: it has more
features available however it consumes more resources and even on recent Android
devices it may not be as good of a user experience as this new Android native
version.

You can find it at [[it.greenaddress.cordova]].
.

Repo Type:git
Repo:https://github.com/greenaddress/GreenBits

Build:1.87_testnet,87
    commit=r1.87
    subdir=app
    gradle=btctestnet
    forcevercode=yes
    prebuild=sed -i -e 's/enable true/enable false/g' build.gradle && \
        sed -i -e 's/output.versionCodeOverride =/0 + /g' build.gradle
    scanignore=bitcoinj/core/src/main/resources/org/bitcoinj/crypto/cacerts,bitcoinj/core/src/test,app/src/btctestnet/assets/checkpoints
    scandelete=app/src/production/assets/checkpoints
    build=JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::") sh ./prepare_fdroid.sh
    ndk=r12b

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.88
Current Version Code:188
